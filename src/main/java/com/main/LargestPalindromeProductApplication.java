package com.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.service.ICalculoService;

@SpringBootApplication(scanBasePackages= {"com.*"})
public class LargestPalindromeProductApplication implements CommandLineRunner{
	
	private   Logger logger = LoggerFactory.getLogger(LargestPalindromeProductApplication.class);
	
	@Autowired
	private ICalculoService calculoService;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(LargestPalindromeProductApplication.class);
		//app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.debug("<-- inicio -->");
		calculoService.calcularPalindromo();
		 logger.debug("<-- fin -->");
	}
}
