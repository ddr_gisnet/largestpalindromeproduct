package com.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.model.Case;
import com.service.ICalculoService;

@Service
public class CalculoService implements ICalculoService {
	
	private   Logger logger = LoggerFactory.getLogger(CalculoService.class);
	
	private Integer T;
	
	private Boolean salir=Boolean.FALSE;
	
	private List<Case> listaPalindromo;
	
	private Long N;

	@Override
	public void calcularPalindromo() {
		logger.debug("<-- inicio -->");
		
		Scanner scanner = new Scanner(System.in);
		
		listaPalindromo=new ArrayList<>();
		
		logger.debug("Teclea x en cualquier momento para salir");
		do {
			
			do {
				logger.debug("Introduce el valor para T: ");
				String tStr = scanner.nextLine();
				if (tStr.trim().equalsIgnoreCase("x")) {
					salir=Boolean.TRUE;
				}else {
					try {
						T=Integer.parseInt(tStr);
						if (T<1 || T> 100) {
							String msnError="T no puede ser menor que 0 o mayor que 100";
							throw new Exception(msnError);
						}
						break;
					} catch (Exception e) {
						logger.debug("Valor no valido " + e.getMessage());
					}
				}
			}while(!salir && true );
			
			if (salir) {
				break;
			}
			
			for(int i=0;i<T;i++) {
				
				Case elcaso=new Case();
				
				do {
					logger.debug("valores para caso " + i);
					logger.debug("Introduce el valor para N: ");
					
					String nStr = scanner.nextLine();
					if (nStr.trim().equalsIgnoreCase("x")) {
						salir=Boolean.TRUE;
					}else {
						try {
							
							
							N=Long.parseLong(nStr);
							
							if (N > 1000000 || N < 101101) {
								String msnError="N no puede ser mayor que 1000000 o menor que 101101";
								throw new Exception(msnError);
							}
							
							elcaso.setNumero(N);
							
							break;
						} catch (Exception e) {
							logger.debug("Valor no valido " + e.getMessage());
						}
					}
					
				}while(!salir && true );
				
				if (salir) {
					break;
				}
				listaPalindromo.add(elcaso);
			}
			
			if (salir) {
				break;
			}
			
			this.encontrarNumeros(listaPalindromo);
			
			this.imprimirResultados(listaPalindromo);
			
			
		}while(true);
		
		
		 scanner.close();
	
		
		logger.debug("<-- fin -->");

	}
	
	private void encontrarNumeros(List<Case> listaPalindromo) {
		logger.debug("<-- inicio -->");
		
		
		for (Case case1 : listaPalindromo) {
			
			case1.setPalindromos(new ArrayList<>());
			
			for (int j=999;j>=0;j--) {
				for (int i=100;i<=j;i++) {
					String strResult = String.valueOf(i*j);
				
					if(this.esPalindromo(strResult)) {
						Long numPalindromo=Long.valueOf(strResult);
						case1.getPalindromos().add(numPalindromo);
					}
				}
				
			}
		}
		
		logger.debug("<-- fin -->");
	}
	
	private Boolean esPalindromo(String strResult) {
		Boolean result = Boolean.FALSE;
		String reverse = new StringBuffer(strResult).reverse().toString(); 
		
		if (strResult.equals(reverse)) {
			result=Boolean.TRUE;
		}

		return result;
	}
	
	private void imprimirResultados(List<Case> listaPalindromo) {
		
		for (Case case1 : listaPalindromo) {
			
			
			Stream<Long> streamLong= case1.getPalindromos().stream().sorted((n1,n2)-> Long.compare(n1, n2));
			
			case1.setPalindromos(streamLong.collect(Collectors.toList()));
			
			Long palindromoResult = null;
			
			for (Long palindromo: case1.getPalindromos()) {
				if (palindromo < case1.getNumero()) {
					palindromoResult = palindromo;
				}else {
					break;
				}
			}
			
			logger.debug("resultado:"+palindromoResult.toString());
		}
		
		
		
	}

}
