package com.model;

import java.util.List;

public class Case {
	
	private Long A;
	private Long B;
	private Long numero;
	private List<Long> palindromos;
	
	public Long getA() {
		return A;
	}
	public void setA(Long a) {
		A = a;
	}
	public Long getB() {
		return B;
	}
	public void setB(Long b) {
		B = b;
	}
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public List<Long> getPalindromos() {
		return palindromos;
	}
	public void setPalindromos(List<Long> palindromos) {
		this.palindromos = palindromos;
	}
	
	

}
